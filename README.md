[[_TOC_]]

## Introduction
This project contains a set of client functions for the [Authorization API](https://authorization-service-api.web.cern.ch/).

The reusable functions are in the module `authz_api_client.api`.

Some utility scripts wrapping the functions from the `authz_api_client.api` are available in the top level directory.

The `authz_api_client` functions require a user token, which can be obtained by various means. One such possibility is to use the provided module `api_token`, which contains code allowing to obtain a token from client credentials.
Projects reusing the `authz_api_client` can obtain the token by other means such as an exchange of a valid user token for another one, valid for the AuthZ API audience. See also: [Authz documentation of token exchange](https://auth.docs.cern.ch/user-documentation/oidc/exchange-for-api/).



## Usage

In order to reuse this module, install it first with the following command:

```bash
python -m pip install "git+https://gitlab.cern.ch/webservices/authz-api-client.git#egg=authz_api_client"
```

Alternatively if poetry is used for dependency management in the target project, this module can be added as a dependency with the following command:

```bash
poetry add git+https://gitlab.cern.ch/webservices/authz-api-client -vvv
```

> Note: the `-vvv` verbose flag is needed due to a bug in poetry, which doesn't prompt for GitLab username/password on consecutive attempts to download the content from a protected repository, unless run in the verbose mode. Therefore if git credential cache is disabled and this flag is not specified, the package installation will fail. In the future we may expose this repo to public, which should allow to avoid this problem.

## Available scripts

Many operations are available directly in the `https://gitlab.cern.ch/webservices/authz-api-client` repo via utility scripts. These allow simple atomic operations such as:

- Creating a role
- Linking a group to a role
- Deleting a role
- Updating a role (e.g. change of LOA)
- Updating the administrators group for an application
- Updating the category of an application
- Getting information about an application
- Getting roles and groups associated with them for an application


## Available API operations

- Application:
  - Get details of an application registration
  - Get / set application/site category
  - Get / set application/site admin group
- Role:
  - Get details of a role
  - Create a role
  - Link a group to a role
  - Delete a role
  - Update a role (e.g. change of LOA)
- Groups:
  - Get an id of a group

Note: the API doesn't support a change in the role name. Therefore one has to create a new role (and associate it with the required groups) and delete the old role.

Note: Add a new function if the one you need doesn't exist.

## List of utility scripts

General note: all scripts require $applicationId (`appId`) as one of the input arguments. It is generated and stored by the CERN's Application Portal. It can be retrieved by a call to `api.get_application_by_name` with the `application name` passed as parameter.

CLIENT_ID and SECRET environment variables must be set prior to the execution of any of the scripts. They are used by the `api_token` module to generate an Authz API token.

### create_role

Usage:

`python3 create_role.py --client $CLIENT_ID --secret $SECRET --appId $applicationId --role $roleName -- description $description --loa $LOA --required false --multifactor false --applyToAllUsers false`

The following arguments are required: --client, --secret, --appId, --role, --description, --loa

```bash
  --client CLIENT       The client_id of the caller
  --secret SECRET       The secret
  --appId APPID         The id of the application
  --role ROLE           The name of the role
  --description DESCRIPTION
                        The role description
  --loa LOA             The LOA to be set (one of: SocialAccount, VerifiedExternal, HEPTrusted, EduGain, CERN)
  --required REQUIRED   If the role is required (default true)
  --multifactor MULTIFACTOR
                        If users must authenticate with Multifactor (default false)
  --applyToAllUsers APPLYTOALLUSERS
                        If the role is applicable to all users (default true)
```

### delete_role

Usage:

`python3 delete_role.py --client $CLIENT_ID --secret $SECRET --appId $applicationId --role $roleName`

All the above arguments are required.

```bash
  --client CLIENT  The client_id of the caller
  --secret SECRET  The secret
  --appId APPID    The id of the application
  --role ROLE      The name of the role to delete
```

### link_group_with_role

Usage:

`python3 link_group_with_role.py --client $CLIENT_ID --secret $SECRET --appId $applicationId --role $roleName --group $groupName`

All the above arguments are required.

```bash
  --client CLIENT  The client_id of the caller
  --secret SECRET  The secret
  --appId APPID    The id of the application
  --role ROLE      The name of the role
  --group GROUP    The name of the group
```

### set_admin_group

Usage:

`python3 set_admin_group.py --client $CLIENT_ID --secret $SECRET --appId $applicationId --group $groupName`

The script expects the following input:

```bash
  --client CLIENT  The client_id of the caller
  --secret SECRET  The secret
  --appId APPID    The id of the application for which the role is defined
  --group GROUP    Egroup to be set as the administrator group of the application
```

### set_category

Sets category of an application identified by appId.

Usage:

`python3 set_category.py --client $CLIENT_ID --secret $SECRET --appId $applicationId --category $category`

The script expects the following input:

```bash
  --client CLIENT      The client_id of the caller
  --secret SECRET      The secret
  --appId APPID        The id of the application for which the role is defined
  --category CATEGORY  The category to be set (Official, Personal, Test)
```

Example call:

`python3 category.py --client $CLIENT_ID --secret $SECRET --appId 08d88047-e03c-4543-82ad-adc2ebde6525 --category Personal`

### set_role_loa

Sets the Level of Assurance (LOA) for a specified role (role name) for an application identified by appId. In addition to changing the LOA itself, the role description is also modified accordingly.

Usage:

`python3 set_role_loa.py --client $CLIENT_ID --secret $SECRET --appId $applicationId --role $roleName --loa $LOA`

The script expects the following input:

```bash
  --client CLIENT  The client_id of the caller
  --secret SECRET  The secret
  --appId APPID    The id of the application for which the role is defined
  --role ROLE      The name of the role to be updated
  --loa LOA        The LOA to be set (one of: SocialAccount , VerifiedExternal, HEPTrusted, EduGain, CERN)
```

Example call:

`python3 set_role_loa.py --client $CLIENT_ID --secret $SECRET --appId 08d87018-647b-453e-83d4-27ed0a0f2d9b --role default-role --loa EduGain`

### get_application

Gets and prints the details of application registration in the Application Portal.

Usage:

`python3 get_application.py --client $CLIENT_ID --secret $SECRET --site $siteName --cluster $clusterName`

The script expects the following input:

```bash
  --client CLIENT  The client_id of the caller
  --secret SECRET  The secret
  --site SITE      The name of the site
  --cluster CLUSTER  The hosting cluster (webeos, webeos-stg)
```

Example call:

`python3 get_application.py --client $CLIENT_ID --secret $SECRET --site test-aw-2 --cluster webeos`

### get_roles_and_groups

Gets and prints the details of application registration, its associated roles and their linked groups.

Usage:

`python3 get_roles_and_groups.py --client $CLIENT --secret $SECRET --site $siteName --cluster $clusterName`

The script expects the following input:

```bash
  --client CLIENT  The client_id of the caller
  --secret SECRET  The secret
  --site SITE      The name of the site
  --cluster CLUSTER  The hosting cluster (webeos, webeos-stg)
```

Example call:

`python3 get_roles_and_groups.py --client $CLIENT --secret $SECRET --site test-aw-2 --cluster webeos`
