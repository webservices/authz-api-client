#!/usr/bin/python
# Sets the Level of Assurance (LOA) for a role for an application in the Application Portal
from argparse import ArgumentParser
from authz_api_client.api_token import get_api_token
from authz_api_client.api import set_role_loa


API_BASE_URL = "https://authorization-service-api.web.cern.ch/api/v1.0"


parser = ArgumentParser()

parser.add_argument("--client", required=True, help="The client_id of the caller")
parser.add_argument("--secret", required=True, help="The secret")
parser.add_argument(
    "--appId", required=True, help="The id of the application for which the role is defined"
)
parser.add_argument(
    "--role", required=True, help="The name of the role to be updated"
)
parser.add_argument(
    "--loa", required=True, help="The LOA to be set (one of: SocialAccount, VerifiedExternal, HEPTrusted, EduGain, CERN)"
)

if __name__ == "__main__":
    args = parser.parse_args()
    api_token = get_api_token(
       args.client, args.secret
    )

    set_role_loa(args.appId, args.role, args.loa, api_token)
    