#!/usr/bin/python
# Links group with a role for an application in the Application Portal
from argparse import ArgumentParser
from authz_api_client.api_token import get_api_token
from authz_api_client.api import link_group_with_role


API_BASE_URL = "https://authorization-service-api.web.cern.ch/api/v1.0"


parser = ArgumentParser()

parser.add_argument("--client", required=True, help="The client_id of the caller")
parser.add_argument("--secret", required=True, help="The secret")
parser.add_argument(
    "--appId", required=True, help="The id of the application"
)
parser.add_argument(
    "--role", required=True, help="The name of the role"
)
parser.add_argument(
    "--group", required=True, help="The name of the group"
)

if __name__ == "__main__":
    args = parser.parse_args()
    api_token = get_api_token(
       args.client, args.secret
    )

    link_group_with_role(args.appId, args.role, args.group, api_token)
        