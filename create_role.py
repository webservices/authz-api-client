#!/usr/bin/python
# Creates a role for an application in the Application Portal
from argparse import ArgumentParser
from authz_api_client.api_token import get_api_token
from authz_api_client.api import create_role


API_BASE_URL = "https://authorization-service-api.web.cern.ch/api/v1.0"


parser = ArgumentParser()

parser.add_argument("--client", required=True, help="The client_id of the caller")
parser.add_argument("--secret", required=True, help="The secret")
parser.add_argument(
    "--appId", required=True, help="The id of the application"
)
parser.add_argument(
    "--role", required=True, help="The name of the role"
)
parser.add_argument(
    "--description", required=True, help="The role description"
)
parser.add_argument(
    "--loa", required=True, help="The LOA to be set (one of: SocialAccount, VerifiedExternal, HEPTrusted, EduGain, CERN)"
)
parser.add_argument(
    "--required", 
    required=False, 
    help="If the role is required (default true)",
    default="true"
)
parser.add_argument(
    "--multifactor", 
    required=False, 
    help="If users must authenticate with Multifactor (default false)",
    default="false"
)
parser.add_argument(
    "--applyToAllUsers", 
    required=False, 
    help="If the role is applicable to all users (default true)",
    default="true"
)


if __name__ == "__main__":
    args = parser.parse_args()
    api_token = get_api_token(
       args.client, args.secret
    )

    create_role(args.appId, args.role, args.role, args.description, args.required, args.loa, args.multifactor, args.applyToAllUsers, api_token)
    