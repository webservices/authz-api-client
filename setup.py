from setuptools import setup

setup(
   name="authz_api_client",
   version="1.0",
   description="A python client for CERN Authorization API",
   author="IT-CDA-WF",
   author_email="openshift-admins@cern.ch",
   package_dir={"authz_api_client":"authz_api_client"},
   packages=['authz_api_client'], 
   python_requires=">=3.6",
   install_requires=[
       "pyjwt",
       "requests"
   ],
)