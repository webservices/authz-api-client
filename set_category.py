#!/usr/bin/python
# Sets the category for an application in the Application Portal
from argparse import ArgumentParser
from authz_api_client.api_token import get_api_token
from authz_api_client.api import set_category

API_BASE_URL = "https://authorization-service-api.web.cern.ch/api/v1.0"

parser = ArgumentParser()

parser.add_argument("--client", required=True, help="The client_id of the caller")
parser.add_argument("--secret", required=True, help="The secret")
parser.add_argument(
    "--appId", required=True, help="The id of the application for which the role is defined"
)
parser.add_argument(
    "--category", required=True, help="The category to be set (Official, Personal, Test)"
)

if __name__ == "__main__":
    args = parser.parse_args()
    api_token = get_api_token(
       args.client, args.secret
    )

    set_category(args.appId, args.category, api_token)
    
    