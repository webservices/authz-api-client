import json
import requests
from pprint import pprint
from datetime import datetime
from urllib.parse import urljoin

API_BASE_URL = "https://authorization-service-api.web.cern.ch/api/v1.0"


loa_mapping = {
    "SocialAccount": {
        "displayName" : "Social Account",
        "id":"f0000000-0000-0000-0000-0000000000b1",
        "description": "Users must authenticate with at least a Social account or CERN lightweight account without a CERN guarantor"
    },
    "VerifiedExternal": {
        "displayName" : "Verified External",
        "id":"f0000000-0000-0000-0000-0000000000b2",
        "description": "Users must authenticate with at least a CERN lightweight account with a CERN guarantor"
    },
    "HEPTrusted": {
        "displayName" : "HEP Trusted",
        "id":"f0000000-0000-0000-0000-0000000000b3",
        "description": "Users must authenticate with at least a Federated HEP account"
    },
    "EduGain": {
        "displayName" : "EduGain with SIRTIFI",
        "id":"f0000000-0000-0000-0000-0000000000b4",
        "description": "Users must authenticate with at least an EduGain federated account compliant with SIRTFI standards"
    },
    "CERN": {
        "displayName" : "CERN",
        "id":"f0000000-0000-0000-0000-0000000000b5",
        "description": "Users must authenticate with a CERN account"
    }
}

cluster_env = {
    "webeos": "webframeworks-webeos-",
    "webeos-stg": "webframeworks-webeos-stg-",
    "paas": "webframeworks-paas-",
    "paas-stg": "webframeworks-paas-stg-"
}

def get_application_by_name(name, cluster, api_token):
    r = requests.get(
        url=f"{API_BASE_URL}/Application",
        params={"filter": f"applicationIdentifier:{cluster_env[cluster]}{name}"},
        headers={"Authorization": f"Bearer {api_token}",},
    )

    if r.status_code == 200 and r.json()["data"]:
        pprint(r)
        return r.json()["data"]
    else:
        print(f"Problem getting data for site: {name}")
        pprint(r)
        pprint(r.json())
        message = r.json()["message"]
        raise Exception(f"Problem getting data for site {name}. Message: {message}")


def get_application(site_application_id, api_token):
    r = requests.get(
        url=f"{API_BASE_URL}/Application/{site_application_id}",
        headers={"Authorization": f"Bearer {api_token}",},
    )

    if r.status_code == 200 and r.json()["data"]:
        return r.json()["data"]
    else:
        print(f"Problem getting data for site: {site_application_id}")
        pprint(r)
        pprint(r.json())
        message = r.json()["message"]
        raise Exception(f"Problem getting data for site {site_application_id}. Message: {message}")


def get_application_category(site_application_id, api_token):
    data = get_application(site_application_id, api_token)
    return data["resourceCategory"]


def get_roles(site_application_id, api_token):
    r = requests.get(
        url=f"{API_BASE_URL}/Application/{site_application_id}/roles",
        headers={"Authorization": f"Bearer {api_token}",},
    )

    if r.status_code == 200:
        return r.json()["data"]
    else:
        print(f"Problem getting roles for site: {site_application_id}")
        pprint(r)
        pprint(r.json())
        message = r.json()["message"]


def get_role(site_application_id, role_name, api_token):
    r = requests.get(
        url=f"{API_BASE_URL}/Application/{site_application_id}/roles",
        params={"filter": f"name:{role_name}"},
        headers={"Authorization": f"Bearer {api_token}",},
    )
    if r.status_code == 200 and r.json()["data"]:
        return r.json()["data"][0]
    else:
        print(f"Problem getting the role {role_name} for site: {site_application_id}")
        pprint(r)
        pprint(r.json())
        message = r.json()["message"]


def create_role(site_application_id, role_name, display_name, description, required, role_loa, multifactor, applytoAllUsers, api_token):
    # Use default common settings for roles without group asignment (authorization based only on LOA) when some parameters are not supplied:
    if required is None:
        required = "true"
    if multifactor is None:
        multifactor = "false"
    if applytoAllUsers is None:
        applytoAllUsers = "true"
    if display_name is None:
        display_name = role_name

    role_data = {
        "name": role_name,
        "displayName": display_name,
        "description": description,
        "applicationId": site_application_id,
        "required": required,
        "minimumLoaId": loa_mapping[role_loa]["id"],
        "multifactor": multifactor,
        "applyToAllUsers": applytoAllUsers
    }
    r = requests.post(
        url=f"{API_BASE_URL}/Application/{site_application_id}/roles",
        headers={
            "Authorization": f"Bearer {api_token}",
            "Content-Type": "application/json-patch+json",
        },
        data=json.dumps(role_data),
    )
    if r.status_code == 200:
        id = r.json()["data"]["id"]
        print(f"Role created successfully:{id}")
        return id
    else:
        print(f"Problem creating role {role_name}")
        pprint(r.json())
        message = r.json()["message"]
        raise Exception(f"Problem creating role {role_name}. Message: {message}")


def delete_role(site_application_id, role_name, api_token):
    role_object = get_role(site_application_id, role_name, api_token)
    if role_object:
        role_id = role_object["id"]
        r = requests.delete(
            url=f"{API_BASE_URL}/Application/{site_application_id}/roles/{role_id}",
            headers={"Authorization": f"Bearer {api_token}"},
        )
        if r.status_code == 200:
            print(f"Role {role_name} for site: {site_application_id} was deleted.")
            return True
        else:
            print(
                f"Problem deleting {role_name} role for site: {site_application_id}"
            )
            pprint(r.json())
            message = r.json()["message"]
            raise Exception(
                f"Problem deleting {role_name} role for site: {site_application_id}. Message: {message}"
            )


def update_role(site_application_id, role_id, role_object, api_token):
    r = requests.put(
        url=f"{API_BASE_URL}/Application/{site_application_id}/roles/{role_id}",
        headers={
            "Authorization": f"Bearer {api_token}",
            "Content-Type": "application/json-patch+json",
        },
        data=json.dumps(role_object),
    )
    if r.status_code == 200:
        print(f"Role {role_id} for site: {site_application_id} was updated")
        return True
    else:
        print(f"Problem updating the role {role_id} for site: {site_application_id}")
        pprint(r.json())
        message = r.json()["message"]
        raise Exception(
            f"Problem updating the role {role_id} for site: {site_application_id}. Message: {message}"
        )


def set_role_loa(site_application_id, role_name, role_loa, api_token):
    role_object = get_role(site_application_id, role_name, api_token)
    if role_object:
        role_id = role_object["id"]
        # prepare the update
        role_object["description"] = loa_mapping[role_loa]["description"]
        role_object["minimumLoaId"] = loa_mapping[role_loa]["id"]
        role_object["modificationTime"] = datetime.now().isoformat()
        update_role(site_application_id, role_id, role_object, api_token)


def get_group_id(group_name, api_token):
    r = requests.get(
        url=f"{API_BASE_URL}/Group",
        params={"filter": f"groupIdentifier:{group_name}"},
        headers={"Authorization": f"Bearer {api_token}"},
    )
    if r.status_code == 200:
        if r.json()["data"]:
            id = r.json()["data"][0]["id"]
            print(f"Group {group_name} found with id {id}")
            return id
        else:
            print(f"Group {group_name} not found.")
            pprint(r.json())
            message = r.json()["message"]
            raise Exception(f"Group {group_name} not found. Message: {message}")


def get_group_name(group_id, api_token):
    r = requests.get(
        url=f"{API_BASE_URL}/Group/{group_id}",
        headers={"Authorization": f"Bearer {api_token}",},
    )
    if r.status_code == 200 and r.json()["data"]:
        group_name = r.json()["data"]["groupIdentifier"]
        print(f"Group {group_name} found for group id {group_id}")
        return group_name
    else:
        raise Exception(f"Group with group id {group_id} not found.")


def get_groups_in_role(site_application_id, role_id, api_token):
    r = requests.get(
        url=f"{API_BASE_URL}/Application/{site_application_id}/roles/{role_id}/groups",
        headers={"Authorization": f"Bearer {api_token}",},
    )
    if r.status_code == 200:
        return r.json()["data"]
    else:
        print(f"Problem getting groups for site: {site_application_id} and role: {role_id}")
        pprint(r)
        pprint(r.json())
        message = r.json()["message"]
        raise Exception(
            f"Problem getting groups for site: {site_application_id} and role: {role_id}. Message: {message}"
        )


def link_group_with_role_by_id(group_id, role_id, site_application_id, api_token):
    r = requests.post(
        url=f"{API_BASE_URL}/Application/{site_application_id}/roles/{role_id}/groups/{group_id}",
        headers={
            "Authorization": f"Bearer {api_token}",
            "Content-Type": "application/json-patch+json",
        },
        data="",
    )
    if r.status_code == 200:
        print(f"Group {group_id} successfully linked with role {role_id}")
        return True
    else:
        print(f"Problem linking group {group_id} to role {role_id}")
        pprint(r.json())
        message = r.json()["message"]
        raise Exception(
            f"Problem linking group {group_id} to role {role_id}. Message: {message}"
        )


def link_group_with_role(site_application_id, role_name, group_name, api_token):
    role_object = get_role(site_application_id, role_name, api_token)
    if role_object:
        role_id = role_object["id"]
        group_id = get_group_id(group_name, api_token)
        link_group_with_role_by_id(group_id, role_id, site_application_id, api_token)


def set_admin_group(site_application_id, admin_group, api_token):
    group_id = get_group_id(admin_group, api_token)
    set_admin_group_by_id(site_application_id, group_id, api_token)


def set_admin_group_by_id(site_application_id, admin_group_id, api_token):
    patch_data = [{
        "op": "replace",
        "path": "/administratorsId",
        "value": admin_group_id
    }]

    r = requests.patch(
        url=f"{API_BASE_URL}/Application/{site_application_id}",
        headers={
            "Authorization": f"Bearer {api_token}",
            "Content-Type": "application/json-patch+json",
        },
        data=json.dumps(patch_data),
    )
    if r.status_code == 200:
        print(f"Admin group {admin_group_id} successfully set for application {site_application_id}")
        return True
    else:
        print(f"Problem setting admin group {admin_group_id} for application {site_application_id}")
        pprint(r.json())
        message = r.json()["message"]

        raise Exception(
            f"Problem setting admin group {admin_group_id} for application {site_application_id}. Message: {message}"
        )

def set_category(site_application_id, category, api_token):
    patch_data = [ {
        "op": "replace",
        "path": "/resourceCategory",
        "value": category
    }]
    r = requests.patch(
        url=f"{API_BASE_URL}/Application/{site_application_id}",
        headers={
            "Authorization": f"Bearer {api_token}",
            "Content-Type": "application/json-patch+json",
        },
        data=json.dumps(patch_data),
    )
    if r.status_code == 200:
        print(f"Category {category} successfully set for application {site_application_id}")
        return True
    else:
        print(f"Problem setting category {category} for application {site_application_id}")
        pprint(r.json())
        message = r.json()["message"]

        raise Exception(
            f"Problem setting category {category} for application {site_application_id}. Message: {message}"
        )

def update_application(site_application_id, api_token, **kwargs):
    patch_data = []

    if "owner" in kwargs:
        user_id = get_user_id(kwargs["owner"], api_token)
        patch_data.append(update_patch_template(user_id, "/ownerId", "replace"))
    if "admin_group" in kwargs:
        group_id = None
        if kwargs["admin_group"] is not None:
           group_id = get_group_id(kwargs["admin_group"], api_token)
        patch_data.append(update_patch_template(group_id, "/administratorsId", "replace"))
    if "category" in kwargs:
        patch_data.append(update_patch_template(kwargs["category"], "/resourceCategory", "replace"))
    if "description" in kwargs:
        patch_data.append(update_patch_template(kwargs["description"], "/description", "replace"))

    r = requests.patch(
        url=f"{API_BASE_URL}/Application/{site_application_id}",
        headers={
            "Authorization": f"Bearer {api_token}",
            "Content-Type": "application/json-patch+json",
        },
        json=patch_data,
    )

    if r.status_code == 200:
        print(f"Application {site_application_id} has been updated successfully")
        return True
    else:
        message = r.json()["message"]
        raise Exception(message)


def get_user_id(user_name, api_token):
    r = requests.get(
        url=f"{API_BASE_URL}/Identity",
        params={"filter": f"upn:{user_name}"},
        headers={"Authorization": f"Bearer {api_token}",},
    )

    if r.status_code == 200 and r.json()["data"]:
        id = r.json()["data"][0]["id"]
        print(f"User {user_name} found with id {id}")
        return id
    else:
        raise Exception(f"User {user_name} not found.")


def get_user_name(user_id, api_token):
    r = requests.get(
        url=f"{API_BASE_URL}/Identity",
        params={"filter": f"id:{user_id}"},
        headers={"Authorization": f"Bearer {api_token}",},
    )

    if r.status_code == 200 and r.json()["data"]:
        user_name = r.json()["data"][0]["upn"]
        print(f"User {user_id} found with login name {user_name}")
        return user_name
    else:
        raise Exception(f"User {user_id} not found.")


def update_patch_template(value, path, op):
    return {
        "value": value,
        "path": path,
        "op": op
    }


def search_users(api_token, include_blocked=False, exact=False, limit=None, only_cern_users=True, allow_service_accounts=False, allow_secondary_accounts=False, **kwargs):
    params = {"filter": [], "sort": "upn"}

    if limit is not None:
        params["limit"] = limit

    if only_cern_users:
        params["filter"].append("source:cern")

    query_filter = '' if exact else 'contains:'
    url = f"{API_BASE_URL}/Identity"

    if "login" in kwargs:
        params["filter"].append(f"upn:{query_filter}{kwargs['login']}")
    elif "email" in kwargs:
        url = urljoin(f"{API_BASE_URL}/", f"Identity/by_email/{kwargs['email']}")
    elif "name" in kwargs:
        params["filter"].append(f"displayName:{query_filter}{kwargs['name']}")

    r = requests.get(
        url=url,
        params=params,
        headers={"Authorization": f"Bearer {api_token}",},
    )

    if r.status_code == 200 and r.json()["data"]:
        users = r.json()["data"]
        user_attributes = ('upn', 'description', 'displayName', 'blocked', 'type')
        result = []

        for user in users:
            if not include_blocked and user["blocked"]:
                continue
            if not allow_secondary_accounts and user["type"].lower() == "secondary":
                continue
            elif not allow_service_accounts and user["type"].lower() == "service":
                continue

            result.append({key: value for key, value in user.items() if key in user_attributes})
        return result
    return []


def search_groups(api_token, include_blocked=False, exact=False, limit=None, **kwargs):
    params = {"filter": [], "sort": "groupIdentifier"}

    if limit is not None:
        params["limit"] = limit

    query_filter = '' if exact else 'contains:'

    if "identifier" in kwargs:
        params["filter"].append(f"groupIdentifier:{query_filter}{kwargs['identifier']}")
    elif "display_name" in kwargs:
        params["filter"].append(f"displayName:{query_filter}{kwargs['display_name']}")
    elif "description" in kwargs:
        params["filter"].append(f"description:{query_filter}{kwargs['description']}")

    r = requests.get(
        url=f"{API_BASE_URL}/Group",
        params=params,
        headers={"Authorization": f"Bearer {api_token}",},
    )

    if r.status_code == 200 and r.json()["data"]:
        groups = r.json()["data"]
        group_attributes = ('groupIdentifier', 'description', 'displayName', 'blocked')
        result = []
        for group in groups:
            if not include_blocked and group["blocked"]:
                continue

            result.append({key: value for key, value in group.items() if key in group_attributes})
        return result
    return []
