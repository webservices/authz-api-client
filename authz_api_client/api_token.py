import argparse
from authlib.integrations.requests_client import OAuth2Session
import logging

DEFAULT_SERVER = "auth.cern.ch"
DEFAULT_REALM = "cern"
DEFAULT_REALM_PREFIX = "auth/realms/{}"
DEFAULT_TOKEN_ENDPOINT = "api-access/token"
TARGET_API = "authorization-service-api"


def get_token_endpoint(server=DEFAULT_SERVER, realm=DEFAULT_REALM):
    """
    Gets the token enpdoint path from the args
    """
    return "https://{}/{}/{}".format(
        server, DEFAULT_REALM_PREFIX.format(realm), DEFAULT_TOKEN_ENDPOINT
    )

def get_api_token(
    client_id, client_secret, token_endpoint=get_token_endpoint()
):
    logging.debug(
        "[x] Getting API token as {} for {}".format(client_id, TARGET_API)
    )

    client = OAuth2Session(client_id, client_secret)
    token = client.fetch_token(token_endpoint, grant_type='client_credentials', audience=TARGET_API)

    logging.debug("[x] Token obtained")

    return token["access_token"]


parser = argparse.ArgumentParser()
parser.add_argument("--client_id", help="Your client ID")
parser.add_argument("--client_secret", help="Your client secret")
parser.add_argument(
    "--realm", help="The keycloak realm, default: cern", type=str, default=DEFAULT_REALM
)
parser.add_argument(
    "--server",
    type=str,
    help="The keycloak server, default: {}".format(DEFAULT_SERVER),
    default=DEFAULT_SERVER,
)

if __name__ == "__main__":
    args = parser.parse_args()
    token_endpoint = get_token_endpoint(args.server, args.realm)
    logging.debug(f"[x] Token endpoint: {token_endpoint}")
    api_token = get_api_token(
        args.client_id,
        args.client_secret,
        token_endpoint=token_endpoint,
    )
    print(api_token)
    