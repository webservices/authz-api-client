#!/usr/bin/python
# Gets roles and their linked groups for an application in the Application Portal
from argparse import ArgumentParser
from pprint import pprint
from authz_api_client.api_token import get_api_token
from authz_api_client.api import get_application_by_name, get_roles, get_groups_in_role


API_BASE_URL = "https://authorization-service-api.web.cern.ch/api/v1.0"


parser = ArgumentParser()

parser.add_argument("--client", required=True, help="The client_id of the caller")
parser.add_argument("--secret", required=True, help="The secret")
parser.add_argument(
    "--site", required=True, help="The name of the site"
)
parser.add_argument(
    "--cluster", required=True, help="The hosting cluster (webeos, webeos-stg)"
)


if __name__ == "__main__":
    args = parser.parse_args()
    api_token = get_api_token(
       args.client, args.secret
    )

    data = get_application_by_name(args.site, args.cluster, api_token)
    print("\nSite data: ")
    pprint(data)

    application_id = data[0]["id"]

    roles = get_roles(application_id, api_token)
    print("\nSite roles:")
    pprint(roles)

    for role in roles:
        role_id = role["id"]
        groups = get_groups_in_role(application_id, role_id, api_token)
        print(f"\nGroups for role {role['name']}:")
        pprint(groups)
   
    