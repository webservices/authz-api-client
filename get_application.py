#!/usr/bin/python
# Gets information about an application from the Application Portal
from argparse import ArgumentParser
from pprint import pprint
from authz_api_client.api_token import get_api_token
from authz_api_client.api import get_application_by_name


API_BASE_URL = "https://authorization-service-api.web.cern.ch/api/v1.0"

parser = ArgumentParser()

parser.add_argument("--client", required=True, help="The client_id of the caller")
parser.add_argument("--secret", required=True, help="The secret")
parser.add_argument(
    "--site", required=True, help="The name of the site"
)
parser.add_argument(
    "--cluster", required=True, help="The hosting cluster (webeos, webeos-stg)"
)


if __name__ == "__main__":
    args = parser.parse_args()
    api_token = get_api_token(
       args.client, args.secret
    )

    data = get_application_by_name(args.site, args.cluster, api_token)
    pprint(data)

   
    